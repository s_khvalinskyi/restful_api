import json
import sys
from urllib2 import URLError
from rest_service import service
from nose.tools import assert_equal
from nose.tools import raises
sys.path.append('../rest_service/')


class TestService(object):

    def __init__(self):
        self.url = "https://google.com"
        self.title = "Google"
        self.url2 = "https://outlook.live.com/owa/"
        self.title2 = ""

    def test_get_title(self):
        assert_equal(service.get_title(self.url), self.title)

    @raises(URLError)
    def test_get_title_invalid_url(self):
        invalid_url = "https://ggle.com"
        service.get_title(invalid_url)

    def test_parse_message_empty(self):
        message = ""
        response = json.dumps(service.parse_message(message), sort_keys=False)
        assert_equal(response, "{}")

    def test_parse_message_no_targets(self):
        message = "jh dfjkhkjdfg dfgkjh (jjjjjsdfghiusdfgghidfgiu) http://dfkjdhfgk gsdfg"
        response = json.dumps(service.parse_message(message), sort_keys=False)
        assert_equal(response, "{}")

    def test_parse_message_mentions(self):
        message = "ff gf @user sfd @userTwo"
        response = json.dumps(service.parse_message(message), sort_keys=False)
        assert_equal(response, '{"mentions": ["@user", "@userTwo"]}')

    def test_parse_message_enoticons(self):
        message = "ff gf (smile) sfd (rofl) ffrTwo"
        response = json.dumps(service.parse_message(message), sort_keys=False)
        assert_equal(response, '{"emoticons": ["smile", "rofl"]}')

    def test_parse_message_urls(self):
        message = "ff gf {} sfd {} ffrTwo".format(self.url, self.url2)
        response = json.dumps(service.parse_message(message), sort_keys=False)
        assert_equal(response, '{"links": [{"url": "https://google.com", "title": "Google"}, '
                               '{"url": "https://outlook.live.com/owa/", '
                               '"title": "Outlook.com - Microsoft free personal email"}]}')

    def test_parse_message_multiple_targets(self):
        message = "jh {} dfjkhkj {} dfg {} {} dfgkjh (jjjjjsdfghiusdfgghidfgiu) {} gsdfg {}".format(
            '@userOne', '@userTwo', '(smile)', '(rofl)', self.url, self.url2
        )
        response = json.dumps(service.parse_message(message), sort_keys=False)
        assert_equal(response, '{"mentions": ["@userOne", "@userTwo"], "emoticons": ["smile", "rofl"], '
                               '"links": [{"url": "https://google.com", "title": "Google"}, '
                               '{"url": "https://outlook.live.com/owa/", '
                               '"title": "Outlook.com - Microsoft free personal email"}]}')
