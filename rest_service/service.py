import re
import urllib2
from collections import OrderedDict
from flask import Flask
from flask import json
from flask import request
from flask import Response
app = Flask(__name__)


@app.route('/message', methods=['POST'])
def api_message():
    if request.headers['Content-Type'] == 'text/plain':
        message = request.data
        response = json.dumps(parse_message(message), sort_keys=False)
        return Response(response, status=200, mimetype='application/json')
    else:
        return '415 Unsupported Media Type'


def parse_message(message):
    mentions_list = re.findall(r'@[a-zA-Z]+', message)
    emoticons_list = re.findall(r'\((\w{1,15})\)', message)
    urls_list = re.findall(
        r'[https?:\/\/[\w-]+\.[\w-]+[\w.,@?^=%&amp;:\/~+#-]*[\w@?^=%&amp;:\/~+#-]?',
        message)
    response = OrderedDict()
    if mentions_list:
        response['mentions'] = mentions_list
    if emoticons_list:
        response['emoticons'] = emoticons_list
    if urls_list:
        links_arr = []
        for link in urls_list:
            link_json = OrderedDict()
            link_json['url'] = link
            link_json['title'] = get_title(link)
            links_arr.append(link_json)
        response['links'] = links_arr
    return response


def get_title(url):
    req = urllib2.Request(url)
    response = urllib2.urlopen(req)
    webpage = response.read()
    title = str(webpage).split('<title>')[1].split('</title>')[0]
    return title


if __name__ == '__main__':
    app.run(port=8008)
